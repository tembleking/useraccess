<?php
    /*
        Fede_UserAccess Magento's Extension
        Copyright (C) 2015  Federico Barcelona (fede_rico_94@hotmail.com)
        
        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.
        
        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
        
        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
    */
    
    class Fede_UserAccess_Model_System_Config_Source_Groups
    {
        public function toOptionArray()
        {
            $optionArray = array();
            $groups_collection = Mage::getModel("customer/group")->getCollection();
            foreach($groups_collection as $group)
                $optionArray[] = array("value" => $group->getId(), "label" => $group->getCustomerGroupCode());
            return $optionArray;
        }
        
        public function toArray()
        {
            $optionArray = array();
            $groups_collection = Mage::getModel("customer/group")->getCollection();
            foreach($groups_collection as $group)
                $optionArray[] = array($group->getId() => $group->getCustomerGroupCode());
            return $optionArray;
        }
    
    
    }
?>