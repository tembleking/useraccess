<?php
    /*
        Fede_UserAccess Magento's Extension
        Copyright (C) 2015  Federico Barcelona (fede_rico_94@hotmail.com)
        
        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.
        
        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
        
        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
    */
    
    class Fede_UserAccess_Model_Observer
    {
        /* 
         * Fede_UserAccess_Helper_Data
         */
        private $helper;
        
        public function __construct()
        {
            // Our helper, it contains useful functions that we can use
            $this->helper = Mage::helper("useraccess");
        }
        
        /** 
          * This function handles the session of someone browsing our web
          */
        public function customerSessionInit(Varien_Event_Observer $observer)
        {
            // we get the event
            $event = $observer->getEvent();
            // this is the session of someone browsing our webpages
            $session = $event->getCustomerSession();
            // this is the customer id who's browsing
            $customer_id = $session->getCustomerId();
            // and this is all the information from the customer
            $customer = Mage::getModel("customer/customer")->load($customer_id);
            
            // We get the groups selected in the configuration backend
            $blockedGroups = explode(
                ',', 
                Mage::getStoreConfig('useraccess/general/blocked_groups')
            );
            
            // First of all we need the model
            $model = Mage::getModel("useraccess/useraccess");
            
            // We will try to load the customer.
            $customer_in_database = $model->load($customer_id);
            
            // If we couldn't load the customer, it means he/she isn't blocked because
            // he/she isn't in the database, but if we could, then process it.
            if($customer_in_database->getCustomerId() !== NULL)
            {
                // We get his group and we compare it with our blocked groups list
                if(in_array($customer->getGroupId(), $blockedGroups))
                    // Disconnect him/her
                    $this->disconnectSession($session);
                else
                    // Okay, you are not blocked, let's remove you from the table
                    $customer_in_database->delete();
            }
        }
        
        // Here we will disconnect the user
        private function disconnectSession($session)
        {
            $this->helper->log("Disconnected session: " . $session->getId());
            
            $session->setId(null);
            
            // now you won't be logged in
            $session->setCustomerGroupId(Mage_Customer_Model_Group::NOT_LOGGED_IN_ID); 
            
            // show an error if we can
            $session->addError(
                "As this account is in a special group which has benefits, our staff must validate your information first."
            );
        }
        
        /**
          * This function handles when a customer registers
          */
        public function customerRegistered(Varien_Event_Observer $observer)
        {
            $event = $observer->getEvent();
            // The new customer
            $customer = $event->getCustomer();
            // This is the group id of the customer
            $group_id = $customer->getGroupId();
            
            // We get the groups selected in the configuration backend
            $blockedGroups = explode(',', Mage::getStoreConfig('useraccess/general/blocked_groups'));
            
            // here we check if the group id of the user is in the blocked list
            if(in_array($group_id, $blockedGroups))
            {
                // We access to the database
                $model = Mage::getModel("useraccess/useraccess");
                
                // Here we fill the information to insert into the database
                $data = array(
                    "customer_id" => $customer->getId(),
                    "name" => $customer->getName(),
                    "email" => $customer->getEmail(),
                    "group" => Mage::getModel("customer/group")->load($group_id)->getCustomerGroupCode(),
                    "group_id" => $group_id,
                    "creation_date" => $customer->getCreatedAt()
                );
                
                // Write the information
                $model->setData($data);
                
                // And Save it!
                $model->save();
            }
        }
    }
?>