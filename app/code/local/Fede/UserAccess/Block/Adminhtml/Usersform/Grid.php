<?php
    /*
        Fede_UserAccess Magento's Extension
        Copyright (C) 2015  Federico Barcelona (fede_rico_94@hotmail.com)
        
        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.
        
        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
        
        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
    */
    
    class Fede_UserAccess_Block_Adminhtml_Usersform_Grid extends Mage_Adminhtml_Block_Widget_Grid
    {
        public function __construct()
        {
            parent::__construct();
            $this->setId('contactGrid');
            $this->setDefaultSort('customer_id');
            $this->setDefaultDir('ASC');
            $this->setSaveParametersInSession(true);
        }
        
        protected function _prepareCollection()
        {
            $collection = Mage::getModel('useraccess/useraccess')->getCollection();
            $this->setCollection($collection);
            return parent::_prepareCollection();
        }
        
        protected function _prepareColumns()
        {
            $this->addColumn('customer_id',
            array(
                'header' => 'ID',
                'align' =>'right',
                'width' => '50px',
                'index' => 'customer_id',
            ));
            
            $this->addColumn('name',
            array(
                'header' => 'Name',
                'align' =>'left',
                'index' => 'name',
            ));
            
            $this->addColumn('email',
            array(
                'header' => 'E-Mail',
                'align' =>'center',
                'index' => 'email',
            ));
            
            $this->addColumn('group',
            array(
                'header' => 'Group',
                'align' =>'center',
                'index' => 'group',
            ));
            
            $this->addColumn('creation_date',
            array(
                'header' => 'Creation Date',
                'align' =>'center',
                'index' => 'creation_date',
            ));
            
            return parent::_prepareColumns();
        }
        
        protected function _prepareMassaction()
		{
			$this->setMassactionIdField('customer_id');
			$this->getMassactionBlock()->setFormFieldName('customer_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('verify_user', array(
					 'label'=> Mage::helper('useraccess')->__('Verify User'),
					 'url'  => $this->getUrl('*/adminhtml_usersform/massVerify')
				));
			$this->getMassactionBlock()->addItem('remove_user', array(
					 'label'=> Mage::helper('useraccess')->__('Remove User'),
					 'url'  => $this->getUrl('*/adminhtml_usersform/massRemove'),
					 'confirm' => Mage::helper('useraccess')->__('Are you sure that you want to REMOVE the user forever?')
				));
			return $this;
		}        
    }
?>