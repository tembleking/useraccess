<?php 
    /*
        Fede_UserAccess Magento's Extension
        Copyright (C) 2015  Federico Barcelona (fede_rico_94@hotmail.com)
        
        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.
        
        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
        
        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
    */
    
    $installer = $this;
    
    $installer->startSetup();
    
    $installer->run("
        CREATE TABLE IF NOT EXISTS {$this->getTable('useraccess')} (
            `customer_id` INT(11) NOT NULL,
            `name` VARCHAR(50) NOT NULL,
            `email` VARCHAR(50) NOT NULL,
            `group` VARCHAR(50) NOT NULL,
            `group_id` INT(11) NOT NULL,
            `creation_date` TIMESTAMP NOT NULL,
            PRIMARY KEY (`customer_id`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=MyISAM
    ;
    
    ");
    
    $installer->endSetup();
?>