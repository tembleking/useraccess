<?php
    /*
        Fede_UserAccess Magento's Extension
        Copyright (C) 2015  Federico Barcelona (fede_rico_94@hotmail.com)
        
        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.
        
        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
        
        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
    */
    
    class Fede_Useraccess_Helper_Data extends Mage_Core_Helper_Abstract
    {
        /** 
          * This function allows us to write a log at /var/log/fede_useraccess.log
          * We can even parse formatted strings.
          */
        public function log($text, $args = null)
        {
            Mage::log(sprintf(var_export($text, true), $args), null, "fede_useraccess.log");
        }
    }
?>