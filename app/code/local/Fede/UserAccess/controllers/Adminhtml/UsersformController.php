<?php
    /*
        Fede_UserAccess Magento's Extension
        Copyright (C) 2015  Federico Barcelona (fede_rico_94@hotmail.com)
        
        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.
        
        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
        
        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
    */
    
    class Fede_UserAccess_Adminhtml_UsersformController extends Mage_Adminhtml_Controller_Action
    {
        protected function _initAction()
		{
            $this->loadLayout()->_setActiveMenu('fede_extensions/useraccess')->_addBreadcrumb(Mage::helper("adminhtml")->__("User Access"),Mage::helper("adminhtml")->__("User Access"));
            
            return $this;
        }
		public function indexAction() 
		{
            $this->_title($this->__("Fede Extensions"));
            $this->_title($this->__("User Access"));
            
            $this->_initAction();
            $this->renderLayout();
        }
        
        public function massVerifyAction()
        {
            try {
                $ids = $this->getRequest()->getPost('customer_ids', array());
                foreach ($ids as $id) {
                    $model = Mage::getModel("useraccess/useraccess")->load($id)->delete();
                }
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("User(s) were succesfully verified"));
            }
            catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
            }
            $this->_redirect('*/*/');
        }
        
        public function massRemoveAction()
        {
            try {
                $ids = $this->getRequest()->getPost('customer_ids', array());
                foreach ($ids as $id) {
                    Mage::getModel("customer/customer")->load($id)->delete();
                    Mage::getModel("useraccess/useraccess")->load($id)->delete();
                }
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("User(s) were succesfully removed"));
            }
            catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
            }
            $this->_redirect('*/*/');
        }
        
    }
?>