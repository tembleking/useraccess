# UserAccess #


This extension provides the feature of blocking new users from privileged groups until they are verified.

## Features ##

* Works with Magento 1.9.1.1 (tested only there)
* Blocks the login of a user in a privileged group until it's allowed to login
* You can remove the user or verify it from the backend

## How do I get set up? ##

### Installation ###
1. Disable Cache from ``` System > Cache Managment```
2. Disable Compilation from ``` System > Tools > Compilation ```
3. Copy all the files to the root of your Magento installation using FTP or other method
4. Flush ``` Magento Cache ```and ``` Cache Storage ``` from ``` System > Cache Managment ```
5. Log out from the admin backend, and log in again.
6. You can enable now Compilation and Cache.

### Configuration ###
1. Go to the backend, click the 'System' tab, and at the menu, you will see 'UserAccess'
2. Select the groups where you want to block new users

Tip: You can select multiple groups by holding the CTRL key.

### How to run tests ###
Create a new user with a blocked group, and see if it's added to the backend at the ``` Fede Extensions > UserAccess ``` tab.

### Deployment instructions ###
If you want to edit some code, you will find it at ``` app/code/local/Fede/UserAccess/ ```

### Contribution ###
You can fork this repository, make the changes you want and create a [Pull Request](https://gitlab.com/tembleking/useraccess/merge_requests/new).

If you find some bug, or you have any issue, don't hesitate to write an [Issue Post](https://gitlab.com/tembleking/useraccess/issues/new) (please, don't duplicate issues)

## License ##
Copyright (C) 2015  Federico Barcelona (fede_rico_94@hotmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.